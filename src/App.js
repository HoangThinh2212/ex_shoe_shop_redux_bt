import "./App.css";
import Ex_ShoeShop_Redux from "./Ex_ShoeShop/Ex_ShoeShop_Redux";

function App() {
  return (
    <div className="App">
      <Ex_ShoeShop_Redux />
    </div>
  );
}

export default App;
