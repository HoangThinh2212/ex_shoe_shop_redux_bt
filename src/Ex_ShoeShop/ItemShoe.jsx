import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, VIEW_DETAIL } from "./redux/constant/constant";

class ItemShoe extends Component {
  render() {
    return (
      <div className="col-3 p-1">
        <div className="card text-left h-100">
          <img
            className="card-img-top"
            src={this.props.data.image}
            alt="true"
          />
          <div className="card-body">
            <h4 className="card-title">{this.props.data.name}</h4>
            <button
              className="btn btn-info mr-5"
              onClick={() => {
                this.props.addToCart(this.props.data);
              }}
            >
              Buy
            </button>
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.viewDetail(this.props.data);
              }}
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },
    viewDetail: (shoe) => {
      dispatch({
        type: VIEW_DETAIL,
        payload: shoe,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
